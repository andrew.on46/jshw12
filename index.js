const images = document.querySelectorAll(".image-to-show");
const stopBtn = document.querySelector(".button1");
const continueBtn = document.querySelector(".button2");

let current = 0;

const autoplayFunc = (start, index) => {
  for (let i = start; i < images.length; i++) {
    images[i].style.display = "none";
  }
  images[index].style.display = "block";
};

autoplayFunc(1, 0);

const intervalFunc = () => {
  myInterval = setInterval(() => {
    current >= images.length - 1 ? (current = 0) : current++;
    autoplayFunc(0, current);
  }, 3000);
};

intervalFunc();

stopBtn.onclick = function () {
  clearInterval(myInterval);
};

continueBtn.onclick = function () {
  intervalFunc();
};
